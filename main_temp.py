
from pandas import read_csv
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import numpy as np
import csv

#get data
def GetData(fileName):
    data = read_csv(fileName, header=0, parse_dates=[0], index_col=0).sort_values(by='Date')
    return data.values


def GetDataNew(fileName):
    with open(fileName) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        data = []
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                print(f'\t Date: {row[0]}, Currency rate: {row[1]}.')
                line_count += 1
                data.append(row)
                #todo: sort by date!!! 
        print(f'Processed {line_count} lines.')

        return data

#Function that calls ARIMA model to fit and forecast the data
def StartARIMAForecasting(Actual, P, D, Q):
	model = ARIMA(Actual, order=(P, D, Q))
	model_fit = model.fit(disp=0)
	prediction = model_fit.forecast()[0]
	return prediction
    
#Get exchange rates
#ActualData = GetData('usd_rub_exchange.csv')

ActualData = GetDataNew('usd_rub_exchange.csv')

#Size of exchange rates
NumberOfElements = len(ActualData)

#Use 70% of data as training, rest 30% to Test model
TrainingSize = int(NumberOfElements * 0.7)
TrainingData = ActualData[0:TrainingSize]
TestData = ActualData[TrainingSize:NumberOfElements]

#new arrays to store actual and predictions
Actual = []

for x in TrainingData:
    Actual.append(np.ndarray([x[1]],np.float))


Predictions = list()

#print('***test***')
#print(ActualData[1])
#for row in TestData:
#    print('Date=%s, Currency=%s' % (row, row))
#print('***test_END***')

#in a for loop, predict values using ARIMA model
for timepoint in range(len(TestData)):
	ActualValue =  TestData[timepoint]
	#forcast value
	Prediction = StartARIMAForecasting(Actual, 3,1,0)    
	print('Date=%s, Actual=%f, Predicted=%f' % ('1111-11-11', ActualValue, Prediction))
	#add it in the list
	Predictions.append(Prediction)
	Actual.append(ActualValue)

#Print MSE to see how good the model is
Error = mean_squared_error(TestData, Predictions)
print('Test Mean Squared Error (smaller the better fit): %.3f' % Error)

# plot
pyplot.plot(TestData)
pyplot.plot(Predictions, color='red')
pyplot.show()


#view top 10 records
#print(exchangeRatesSeries.head(10))

#stats
#print(exchangeRatesSeries.describe())

#plot view
#exchangeRatesSeries.plot(color='green')

#hystograme
#exchangeRatesSeries.hist()

#import csv
#
#with open('employee_birthday.txt') as csv_file:
#    csv_reader = csv.reader(csv_file, delimiter=',')
#    line_count = 0
#    for row in csv_reader:
#        if line_count == 0:
#            print(f'Column names are {", ".join(row)}')
#            line_count += 1
#        else:
#            print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
#            line_count += 1
#    print(f'Processed {line_count} lines.')